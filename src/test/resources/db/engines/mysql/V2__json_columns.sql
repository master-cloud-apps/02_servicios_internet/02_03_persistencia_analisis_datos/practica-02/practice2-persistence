ALTER TABLE `flight`
    ADD COLUMN cabin_crew_info JSON;

UPDATE `flight`
SET cabin_crew_info = JSON_OBJECT('cabin_crew', JSON_ARRAY(7275, 7276))
WHERE id = 7274;

UPDATE `flight`
SET cabin_crew_info = JSON_OBJECT('cabin_crew', JSON_ARRAY(7275, 7276))
WHERE id = 7277;

UPDATE `flight`
SET cabin_crew_info = JSON_OBJECT('cabin_crew', JSON_ARRAY(7279, 7280))
WHERE id = 7278;

UPDATE `flight`
SET cabin_crew_info = JSON_OBJECT('cabin_crew', JSON_ARRAY(7282))
WHERE id = 7281;

UPDATE `flight`
SET cabin_crew_info = JSON_OBJECT('cabin_crew', JSON_ARRAY(7282, 7284))
WHERE id = 7283;

ALTER TABLE `plane`
    ADD COLUMN maintenance_info JSON;

UPDATE `plane`
SET maintenance_info = JSON_OBJECT('maintenances', JSON_ARRAY(
        JSON_OBJECT('maintenance_id', 7267, 'mechanic_id', 7266),
        JSON_OBJECT('maintenance_id', 7268, 'mechanic_id', 7264),
        JSON_OBJECT('maintenance_id', 7269, 'mechanic_id', 7266)))
WHERE id = 7258;

UPDATE `plane`
SET maintenance_info = JSON_OBJECT('maintenances', JSON_ARRAY(
        JSON_OBJECT('maintenance_id', 7270, 'mechanic_id', 7265),
        JSON_OBJECT('maintenance_id', 7271, 'mechanic_id', 7264)))
WHERE id = 7257;

UPDATE `plane`
SET maintenance_info = JSON_OBJECT('maintenances', JSON_ARRAY(
        JSON_OBJECT('maintenance_id', 7272, 'mechanic_id', 7265)))
WHERE id = 7260;