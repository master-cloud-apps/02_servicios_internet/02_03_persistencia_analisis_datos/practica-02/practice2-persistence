package es.urjc.code.practice.services;

import es.urjc.code.practice.dtos.MechanicDTO;
import es.urjc.code.practice.dtos.PlaneDTO;
import es.urjc.code.practice.utils.EntitiesLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.text.ParseException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.util.CollectionUtils.isEmpty;


@Disabled
@SpringBootTest
@ActiveProfiles(value = {"mysql"})
class PlaneServiceMySQLTest {

    @Autowired
    private PlaneService planeService;
    @Autowired
    private EntitiesLoader prepareDatabase;

    @BeforeEach
    void setUp() throws ParseException {
        this.prepareDatabase.setUp();
    }

    @AfterEach
    void tearDown() {
        this.prepareDatabase.deleteAll();
    }

    @Test
    void givenPlanes_whenFindPlanesAndMechanicsFromJSON_thenShouldReturnPlanesAndMechanics() {

        List<PlaneDTO> listPlanes = this.planeService.findPlanesAndMechanicsFromJSON();

        assertEquals(4, listPlanes.size());

        assertEquals(2, this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_1, listPlanes).getMechanics().size());
        assertThat(this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_1, listPlanes).getMechanics(),
                containsInAnyOrder(new MechanicDTO(
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_7).getName(),
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_7).getSurname()),
                        new MechanicDTO(
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_9).getName(),
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_9).getSurname())
                ));

        assertEquals(2, this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_2, listPlanes).getMechanics().size());
        assertThat(this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_2, listPlanes).getMechanics(),
                containsInAnyOrder(new MechanicDTO(
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_8).getName(),
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_8).getSurname()),
                        new MechanicDTO(
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_9).getName(),
                                this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_9).getSurname())
                ));

        assertEquals(1, this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_3, listPlanes).getMechanics().size());
        assertEquals(this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_3, listPlanes).getMechanics().get(0),
                new MechanicDTO(
                        this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_8).getName(),
                        this.prepareDatabase.getMechanicByEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_8).getSurname()));

        assertTrue(isEmpty(this.getPlaneDTOByRegistration(EntitiesLoader.PLANE_REGISTRATION_4, listPlanes).getMechanics()));

    }

    private PlaneDTO getPlaneDTOByRegistration(String registration, List<PlaneDTO> planes) {
        return planes
                .stream()
                .filter(plane -> plane.getRegistration().equals(registration))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

}
