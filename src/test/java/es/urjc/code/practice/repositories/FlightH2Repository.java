package es.urjc.code.practice.repositories;

import es.urjc.code.practice.models.Flight;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Profile("h2")
@Repository
public interface FlightH2Repository extends FlightRepository {

    @Query(value = "select f FROM Flight f WHERE f.destinationAirport.city = ?1 AND FORMATDATETIME(f.departureDate, " +
            "'dd/MM/y') = ?2 ORDER BY f.departureDate")
    List<Flight> findFlightsByDestinationCityAndDate(String city, String date);

}
