package es.urjc.code.practice;

import es.urjc.code.practice.services.LoggerService;
import es.urjc.code.practice.services.MockServiceFactory;
import es.urjc.code.practice.services.ProvinceService;
import es.urjc.code.practice.services.impl.LoggerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DataLoggerTest {

    private DataLogger dataLogger;
    private ProvinceService provinceService = new MockServiceFactory().getMockProvinceService();
    private LoggerService loggerService = new LoggerServiceImpl();

    @BeforeEach
    void setUp() {
        this.dataLogger = new DataLogger(this.provinceService, this.loggerService);
    }

    @Test
    void should_write_all_info() {
        this.dataLogger.run();
    }
}
