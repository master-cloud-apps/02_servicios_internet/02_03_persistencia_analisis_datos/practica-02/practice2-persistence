package es.urjc.code.practice.services;

import es.urjc.code.practice.dtos.PlaneDTO;
import es.urjc.code.practice.models.Plane;

import java.util.List;

public interface PlaneService {

    List<PlaneDTO> findPlanesAndMechanics();

    List<Plane> findAll();

    List<PlaneDTO> findPlanesAndMechanicsFromJSON();
}
