package es.urjc.code.practice.services;

import es.urjc.code.practice.dtos.CAProvince;
import es.urjc.code.practice.models.Province;

import java.util.List;

public interface ProvinceService {
    List<Province> getAllProvinces();

    List<CAProvince> getCA();
}
