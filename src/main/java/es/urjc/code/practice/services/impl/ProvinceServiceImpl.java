package es.urjc.code.practice.services.impl;

import es.urjc.code.practice.dtos.CAProvince;
import es.urjc.code.practice.models.Province;
import es.urjc.code.practice.repositories.ProvinceRepository;
import es.urjc.code.practice.services.ProvinceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProvinceServiceImpl implements ProvinceService {
    private final ProvinceRepository provinceRepository;

    public ProvinceServiceImpl(ProvinceRepository provinceRepository) {
        this.provinceRepository = provinceRepository;
    }

    @Override
    public List<Province> getAllProvinces() {
        return this.provinceRepository.findAll();
    }

    @Override
    public List<CAProvince> getCA() {
        return this.provinceRepository.getCA();
    }
}
