package es.urjc.code.practice.services;

public interface LoggerService {

    void logElements(String head, EntitiesFinder entitiesFinder);

    void logOneElement(String head, Object elementToLog);

    void logElement(String head, LogElement consumer);
}
