package es.urjc.code.practice.services;

import java.text.ParseException;

public interface LogElements {

    void logElementsAfterFindThem(EntitiesFinder entitiesFinder) throws ParseException;
}
