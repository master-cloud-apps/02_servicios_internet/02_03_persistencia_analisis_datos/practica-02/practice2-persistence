package es.urjc.code.practice;

import es.urjc.code.practice.services.CabinCrewService;
import es.urjc.code.practice.services.FlightService;
import es.urjc.code.practice.services.LoggerService;
import es.urjc.code.practice.services.PlaneService;
import es.urjc.code.practice.utils.EntitiesLoader;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;


@Controller
@Profile("practice1")
public class Practice1Runner implements CommandLineRunner {

    private final EntitiesLoader entitiesLoader;
    private final PlaneService planeService;
    private final FlightService flightService;
    private final CabinCrewService cabinCrewService;
    private final LoggerService loggerService;

    public Practice1Runner(EntitiesLoader entitiesLoader,
                           PlaneService planeService,
                           FlightService flightService,
                           CabinCrewService cabinCrewService,
                           LoggerService loggerService) {
        this.entitiesLoader = entitiesLoader;
        this.planeService = planeService;
        this.flightService = flightService;
        this.cabinCrewService = cabinCrewService;
        this.loggerService = loggerService;
    }

    @Override
    public void run(String... args) {
        this.loggerService.logElements("Showing airplanes with their mechanics", this.planeService::findPlanesAndMechanics);

        this.loggerService.logElements("Showing flight by destination city and departure date sorted by hours",
                () -> this.flightService
                        .findFlightsByDestinationCityInDate(EntitiesLoader.CITY_3,
                                this.entitiesLoader.getSdf().parse(EntitiesLoader.DEPARTURE_DATE_1_WITH_HOUR)));

        this.loggerService.logOneElement("Showing cabin crew by employee code with origin cities and departure date",
                this.cabinCrewService.findCitiesAndDatesByCrewEmployeeCode(EntitiesLoader.EMPLOYEE_CODE_1));

        this.loggerService.logElements("Showing all cabin crew with their total number of flights and their total flight hours",
                this.cabinCrewService::findAllCrewWithTotalFlights);
    }



}
