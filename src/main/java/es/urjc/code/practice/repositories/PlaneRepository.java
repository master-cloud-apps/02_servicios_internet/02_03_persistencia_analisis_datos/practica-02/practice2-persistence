package es.urjc.code.practice.repositories;

import es.urjc.code.practice.dtos.FlatPlaneWithMechanicDTO;
import es.urjc.code.practice.models.Plane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PlaneRepository extends JpaRepository<Plane, Long> {

    @Query(name = "plane.findPlaneMechanics", nativeQuery = true)
    List<FlatPlaneWithMechanicDTO> findPlaneMechanics();

    @Query(name = "plane.findPlaneMechanicsJSON", nativeQuery = true)
    List<FlatPlaneWithMechanicDTO> findPlaneMechanicsJSON();

}

