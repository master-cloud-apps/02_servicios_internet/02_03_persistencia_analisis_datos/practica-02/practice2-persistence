package es.urjc.code.practice.repositories;

import es.urjc.code.practice.dtos.CAProvince;
import es.urjc.code.practice.models.Province;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProvinceRepository extends MongoRepository<Province, String> {

    @Aggregation(pipeline = {"{$group: {_id: $CA, numProvinces: {$sum: 1}}}", "{$sort: {_id: 1}}", "{$project: {_id: " +
            "{$ifNull: [\"$_id\", \"sin comunidad\"]}, numProvinces: 1}}"})
    List<CAProvince> getCA();
}
