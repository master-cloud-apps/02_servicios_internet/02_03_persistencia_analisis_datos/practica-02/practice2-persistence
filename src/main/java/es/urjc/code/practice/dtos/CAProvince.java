package es.urjc.code.practice.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

@Getter
@ToString
@Builder
public class CAProvince {

    private @Id String CA;
    private int numProvinces;
}
