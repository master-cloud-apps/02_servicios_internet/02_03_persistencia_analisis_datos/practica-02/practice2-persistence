package es.urjc.code.practice.dtos;

import lombok.ToString;

import java.util.Date;

@ToString
public class FlightDTO {

    private String originCity;
    private Date departureDate;

    public FlightDTO(String originCity, Date departureDate) {
        this.originCity = originCity;
        this.departureDate = departureDate;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof FlightDTO))
            return false;
        FlightDTO flightDTO = (FlightDTO) o;
        return originCity.equals(flightDTO.originCity)
                && departureDate.getTime() == flightDTO.departureDate.getTime();
    }

}
