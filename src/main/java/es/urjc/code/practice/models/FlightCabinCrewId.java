package es.urjc.code.practice.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class FlightCabinCrewId implements Serializable {

    private Long flightId;
    private Long cabinCrewId;
}
