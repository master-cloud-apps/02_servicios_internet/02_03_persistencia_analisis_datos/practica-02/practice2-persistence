package es.urjc.code.practice.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Maintenance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    @NotNull
    private Plane checkedPlane;

    @NotNull
    private Date initTime;

    @NotNull
    private Date endTime;

    private Float reparationHours;

    @OneToOne
    @NotNull
    private Mechanic mechanic;

    @NotBlank
    private String checkType;

    private String description;

    @OneToOne
    @NotNull
    private Airport airport;

}
