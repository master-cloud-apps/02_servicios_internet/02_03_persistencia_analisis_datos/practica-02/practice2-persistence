package es.urjc.code.practice.models;

import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.validation.constraints.NotNull;

@Entity
@NoArgsConstructor
public class FlightCabinCrew {

    @EmbeddedId
    private FlightCabinCrewId id;

    @ManyToOne
    @MapsId("flightId")
    @NotNull
    private Flight flight;

    @ManyToOne
    @MapsId("cabinCrewId")
    @NotNull
    private CabinCrew cabinCrew;

    public FlightCabinCrew(Flight flight, CabinCrew cabinCrew) {
        this.flight = flight;
        this.cabinCrew = cabinCrew;
        this.id = new FlightCabinCrewId(flight.getId(), cabinCrew.getId());
    }
}
