-- MySQL dump 10.13  Distrib 8.0.21, for macos10.15 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `airport`
--

DROP TABLE IF EXISTS `airport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `airport` (
                           `id` bigint(20) NOT NULL,
                           `iatacode` varchar(3) DEFAULT NULL,
                           `city` varchar(255) DEFAULT NULL,
                           `country` varchar(255) DEFAULT NULL,
                           `name` varchar(255) DEFAULT NULL,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `UK_r1vc1fha84b37bjdrnumo213l` (`iatacode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airport`
--

LOCK TABLES `airport` WRITE;
/*!40000 ALTER TABLE `airport` DISABLE KEYS */;
INSERT INTO `airport` VALUES (7261,'SCQ','Santiago','Spain','Rosalía de Castro - Santiago de Compostela'),(7262,'MAD','Madrid','Spain','Adolfo Suárez Madrid–Barajas'),(7263,'VAL','Valencia','Spain','Manises');
/*!40000 ALTER TABLE `airport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cabin_crew`
--

DROP TABLE IF EXISTS `cabin_crew`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cabin_crew` (
                              `id` bigint(20) NOT NULL,
                              `company_name` varchar(255) DEFAULT NULL,
                              `employee_code` varchar(255) DEFAULT NULL,
                              `name` varchar(255) DEFAULT NULL,
                              `surname` varchar(255) DEFAULT NULL,
                              `position` varchar(255) DEFAULT NULL,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cabin_crew`
--

LOCK TABLES `cabin_crew` WRITE;
/*!40000 ALTER TABLE `cabin_crew` DISABLE KEYS */;
INSERT INTO `cabin_crew` VALUES (7273,'Ryanair','EMP-000','Abel','Hernández','Co-pilot'),(7275,'Vueling','EMP-003','Luis','Perez','Commander'),(7276,'Vueling','EMP-004','Isabel','Ayuso','Co-pilot'),(7279,'Vueling','EMP-005','María','López','Commander'),(7280,'Vueling','EMP-006','Ana','García','Co-pilot'),(7282,'Iberia','EMP-001','Alberto','Eyo','Commander'),(7284,'Iberia','EMP-002','Pelayo','Martín','Co-pilot');
/*!40000 ALTER TABLE `cabin_crew` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight`
--

DROP TABLE IF EXISTS `flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flight` (
                          `id` bigint(20) NOT NULL,
                          `company_name` varchar(255) DEFAULT NULL,
                          `departure_date` datetime(6) DEFAULT NULL,
                          `duration_time` float DEFAULT NULL,
                          `flight_code` varchar(255) DEFAULT NULL,
                          `destination_airport_id` bigint(20) DEFAULT NULL,
                          `origin_airport_id` bigint(20) DEFAULT NULL,
                          `plane_id` bigint(20) DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          KEY `FK6uc5h994cl1g7yxsvnxkilqbl` (`destination_airport_id`),
                          KEY `FKso40hf6mflwfco2iv24gwcwy7` (`origin_airport_id`),
                          KEY `FK7p9fvp6d7uh9cgn47uet8a8nb` (`plane_id`),
                          CONSTRAINT `FK6uc5h994cl1g7yxsvnxkilqbl` FOREIGN KEY (`destination_airport_id`) REFERENCES `airport` (`id`),
                          CONSTRAINT `FK7p9fvp6d7uh9cgn47uet8a8nb` FOREIGN KEY (`plane_id`) REFERENCES `plane` (`id`),
                          CONSTRAINT `FKso40hf6mflwfco2iv24gwcwy7` FOREIGN KEY (`origin_airport_id`) REFERENCES `airport` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight`
--

LOCK TABLES `flight` WRITE;
/*!40000 ALTER TABLE `flight` DISABLE KEYS */;
INSERT INTO `flight` VALUES (7274,'Vueling','2021-01-01 01:00:00.000000',2.15,'VU101',7261,7262,7258),(7277,'Vueling','2021-01-01 03:00:00.000000',2.25,'VU103',7261,7262,7260),(7278,'Vueling','2021-01-01 02:00:00.000000',3.5,'VU102',7261,7263,7257),(7281,'Vueling','2021-01-01 00:00:00.000000',1.05,'IB102',7262,7263,7258),(7283,'Iberia','2021-02-06 13:23:45.000000',1.05,'IB101',7263,7262,7258);
/*!40000 ALTER TABLE `flight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight_cabin_crew`
--

DROP TABLE IF EXISTS `flight_cabin_crew`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flight_cabin_crew` (
                                     `cabin_crew_id` bigint(20) NOT NULL,
                                     `flight_id` bigint(20) NOT NULL,
                                     PRIMARY KEY (`cabin_crew_id`,`flight_id`),
                                     KEY `FKmuum0ja1ayfrqkchtgdom5edy` (`flight_id`),
                                     CONSTRAINT `FK7d008hpbuo49w57563ytvruik` FOREIGN KEY (`cabin_crew_id`) REFERENCES `cabin_crew` (`id`),
                                     CONSTRAINT `FKmuum0ja1ayfrqkchtgdom5edy` FOREIGN KEY (`flight_id`) REFERENCES `flight` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight_cabin_crew`
--

LOCK TABLES `flight_cabin_crew` WRITE;
/*!40000 ALTER TABLE `flight_cabin_crew` DISABLE KEYS */;
INSERT INTO `flight_cabin_crew` VALUES (7275,7274),(7276,7274),(7275,7277),(7276,7277),(7279,7278),(7280,7278),(7282,7281),(7282,7283),(7284,7283);
/*!40000 ALTER TABLE `flight_cabin_crew` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
    `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (7285);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maintenance`
--

DROP TABLE IF EXISTS `maintenance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maintenance` (
                               `id` bigint(20) NOT NULL,
                               `check_type` varchar(255) DEFAULT NULL,
                               `description` varchar(255) DEFAULT NULL,
                               `end_time` datetime(6) DEFAULT NULL,
                               `init_time` datetime(6) DEFAULT NULL,
                               `reparation_hours` float DEFAULT NULL,
                               `airport_id` bigint(20) DEFAULT NULL,
                               `checked_plane_id` bigint(20) DEFAULT NULL,
                               `mechanic_id` bigint(20) DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               KEY `FKeeshfafsmom3ddrgvlqy2g9lm` (`airport_id`),
                               KEY `FK7vdkkwpb7ufew37wdudkxs4y8` (`checked_plane_id`),
                               KEY `FK6t7wc56qahwutin7g5twk7plg` (`mechanic_id`),
                               CONSTRAINT `FK6t7wc56qahwutin7g5twk7plg` FOREIGN KEY (`mechanic_id`) REFERENCES `mechanic` (`id`),
                               CONSTRAINT `FK7vdkkwpb7ufew37wdudkxs4y8` FOREIGN KEY (`checked_plane_id`) REFERENCES `plane` (`id`),
                               CONSTRAINT `FKeeshfafsmom3ddrgvlqy2g9lm` FOREIGN KEY (`airport_id`) REFERENCES `airport` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maintenance`
--

LOCK TABLES `maintenance` WRITE;
/*!40000 ALTER TABLE `maintenance` DISABLE KEYS */;
INSERT INTO `maintenance` VALUES (7267,'reparation','landing gear failure','2020-01-26 18:45:32.000000','2020-01-26 17:15:32.000000',1.5,7263,7258,7266),(7268,'periodic','Periodic maintenance','2020-06-12 17:15:32.000000','2020-06-12 17:15:32.000000',1.5,7262,7258,7264),(7269,'reparation','landing gear failure','2020-06-12 13:00:00.000000','2021-06-12 12:00:00.000000',1,7263,7258,7266),(7270,'periodic','Periodic maintenance','2020-04-22 12:15:32.000000','2020-04-22 11:15:32.000000',1.5,7262,7257,7265),(7271,'reparation','mechanical failures','2020-11-22 13:00:00.000000','2020-11-22 12:00:00.000000',1,7263,7257,7264),(7272,'reparation','mechanical failures','2020-01-16 12:00:10.000000','2020-01-16 09:30:10.000000',2.5,7261,7260,7265);
/*!40000 ALTER TABLE `maintenance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mechanic`
--

DROP TABLE IF EXISTS `mechanic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mechanic` (
                            `id` bigint(20) NOT NULL,
                            `company_name` varchar(255) DEFAULT NULL,
                            `employee_code` varchar(255) DEFAULT NULL,
                            `name` varchar(255) DEFAULT NULL,
                            `surname` varchar(255) DEFAULT NULL,
                            `formation` varchar(255) DEFAULT NULL,
                            `incorporation_year` int(11) DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mechanic`
--

LOCK TABLES `mechanic` WRITE;
/*!40000 ALTER TABLE `mechanic` DISABLE KEYS */;
INSERT INTO `mechanic` VALUES (7264,'Iberia','EMP-009','Samuel','Baquero','Line mechanic',2008),(7265,'Vueling','EMP-008','David','Morata','Superior Technician in Aeromechanical Maintenance',2010),(7266,'Iberia','EMP-007','Alvaro','Martín','Advanced technician',2005);
/*!40000 ALTER TABLE `mechanic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plane`
--

DROP TABLE IF EXISTS `plane`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plane` (
                         `id` bigint(20) NOT NULL,
                         `flight_hours` float DEFAULT NULL,
                         `model` varchar(255) DEFAULT NULL,
                         `producer` varchar(255) DEFAULT NULL,
                         `registration` varchar(255) DEFAULT NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `UK_2owjfqlu3j8ra2upvkvykpoys` (`registration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plane`
--

LOCK TABLES `plane` WRITE;
/*!40000 ALTER TABLE `plane` DISABLE KEYS */;
INSERT INTO `plane` VALUES (7257,320.73,'A340','Airbus','EC-AAB'),(7258,245.73,'747','Boeing','EC-AAA'),(7259,194.37,'CSeries','Bombardier','EC-AAD'),(7260,49.73,'100','Embraer','EC-AAC');
/*!40000 ALTER TABLE `plane` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-13  9:53:15
